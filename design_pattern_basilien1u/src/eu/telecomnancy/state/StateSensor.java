/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.state;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author basilien1u
 */
public class StateSensor extends AbstractSensor{
    AbstractStateSensor sens;
    double value;
    
    public StateSensor(){
        sens=new SensorStateOff(this);
    }
    
    @Override
    public void on(){
        sens.on(this);
    }
    
    @Override
    public void off(){
        sens.off(this);
    }

    @Override
    public boolean getStatus() {
        return sens.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sens.update();
        this.updateViews();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        this.value=sens.getValue();
        return this.value;
    }
    
    public void setEtat(AbstractStateSensor s){
        this.sens=s;
    }
    
}
