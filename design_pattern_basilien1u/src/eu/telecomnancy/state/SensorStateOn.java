/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.state;

import java.util.Random;

/**
 *
 * @author basilien1u
 */
public class SensorStateOn extends AbstractStateSensor {
    private double value;
    
    public SensorStateOn(StateSensor StS){
        this.sensor=StS;
        value=0;
    }
    
    @Override
    public void on(StateSensor StS){}
    
    @Override
    public void off(StateSensor StS){
        AbstractStateSensor aSens=new SensorStateOff(StS);
        this.sensor.setEtat(aSens);
    }
    
    public SensorStateOn(){
        value = (new Random()).nextDouble() * 100;
    }
    
    @Override
    public void update(){
        value = (new Random()).nextDouble() * 100;
    }
    
    @Override
    public boolean getStatus(){
        return true;
    }
    
    @Override
    public double getValue(){
        return value;
    }
}
