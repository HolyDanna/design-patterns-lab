/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author holy
 */
public class SensorStateOff extends AbstractStateSensor{
    
    public SensorStateOff(StateSensor StS){
        this.sensor=StS;
    }
    
    @Override
    public void on(StateSensor StS){
        AbstractStateSensor aSens=new SensorStateOn(StS);
        this.sensor.setEtat(aSens);
    }
    
    @Override
    public void off(StateSensor StS){}
    
    @Override
    public void update() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }
    
    @Override
    public boolean getStatus(){
        return false;
    }
    
    @Override
    public double getValue() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
}
