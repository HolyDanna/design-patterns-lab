/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author holy
 */
public abstract class AbstractStateSensor {
    
    StateSensor sensor;
    
    public abstract void on(StateSensor StS);
    
    public abstract void off(StateSensor StS);

    public abstract boolean getStatus();

    public abstract void update() throws SensorNotActivatedException;
    
    public abstract double getValue() throws SensorNotActivatedException;
    
}
