/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;
import eu.telecomnancy.ui.Observer;
import java.util.ArrayList;

/**
 *
 * @author basilien1u
 */
public interface Observable {
        
    public void attach(Observer Viewer);
    
    public void detach(Observer Viewer);
    
    public void updateViews();
}
