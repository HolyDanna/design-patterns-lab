/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observer;
import java.util.ArrayList;

/**
 *
 * @author basilien1u
 */
public abstract class AbstractSensor implements SensorInterface {
    
    ArrayList<Observer> Viewers = new ArrayList<>();
    
    @Override
        public void attach(Observer Viewer){
        if (!Viewers.contains(Viewer))
            Viewers.add(Viewer);
    }
    
    @Override
    public void detach(Observer Viewer) {
        if (Viewers.contains(Viewer))
            Viewers.remove(Viewer);
    }
    
    @Override
    public void updateViews(){
        for (Observer Viewer : Viewers) {
            Viewer.updateView();
        }
    }
}
