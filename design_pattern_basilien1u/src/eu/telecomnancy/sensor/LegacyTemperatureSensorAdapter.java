/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

/**
 *
 * @author basilien1u
 */

public class LegacyTemperatureSensorAdapter extends AbstractSensor {

    LegacyTemperatureSensor Sens=new LegacyTemperatureSensor();
    
    @Override
    public void on() {
        if (!Sens.getStatus())
            Sens.onOff();
    }

    @Override
    public void off() {
        
        if (Sens.getStatus())
            Sens.onOff();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (Sens.getStatus()){
            this.updateViews();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (Sens.getStatus())
            return Sens.getTemperature();
        else
            throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public boolean getStatus() {
        return Sens.getStatus();
    }
    
}
