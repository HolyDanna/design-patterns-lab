package eu.telecomnancy;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensorAdapter;
import eu.telecomnancy.proxy.SensorProxy;
import eu.telecomnancy.state.StateSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        //AbstractSensor sensor = new LegacyTemperatureSensorAdapter();
        AbstractSensor sensor = new StateSensor();
        sensor= new SensorProxy(sensor);
        new MainWindow(sensor);
    }

}
