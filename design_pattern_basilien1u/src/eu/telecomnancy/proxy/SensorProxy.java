/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.proxy;

import eu.telecomnancy.sensor.AbstractSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author holy
 */
public class SensorProxy extends AbstractSensor {
    DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date d;
    AbstractSensor sens;
    SensorLogger log;
    
    public SensorProxy(AbstractSensor s){
        this.sens=s;
        df.setTimeZone(TimeZone.getDefault());
        log = new SimpleSensorLogger();
    }
    
    @Override
    public void on() {
        d=new Date();
        log.log(LogLevel.INFO,df.format(d)+" - method on() - no return value");
        sens.on();
    }

    @Override
    public void off() {
        d=new Date();
        log.log(LogLevel.INFO,df.format(d)+" - method off() - no return value");
        sens.off();
    }

    @Override
    public boolean getStatus() {
        d=new Date();
        boolean b=sens.getStatus();
        log.log(LogLevel.INFO,df.format(d)+" - method getStatus() - return "+b);
        return b;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        d=new Date();
        log.log(LogLevel.INFO,df.format(d)+" - method update() - no return value");
        sens.update();
        this.updateViews();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        d=new Date();
        double val=sens.getValue();
        log.log(LogLevel.INFO,df.format(d)+" - method getValue() - return "+val);
        return val;
    }
    
}
