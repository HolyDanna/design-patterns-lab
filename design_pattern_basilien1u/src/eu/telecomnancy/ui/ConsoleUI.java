package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConsoleUI {

    private ISensor sensor;
    private Scanner console;

    public ConsoleUI(ISensor sensor) {
        this.sensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();
    }

    public void manageCLI() {
        String rep = "";
        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value");
        while (!"q".equals(rep)) {
            try {
                System.out.print(":> ");
                rep = this.console.nextLine();
                if (null != rep) switch (rep) {
                    case "on":
                    case "o":
                        this.sensor.on();
                        System.out.println("sensor turned on.");
                        break;
                    case "off":
                    case "O":
                        this.sensor.off();
                        System.out.println("sensor turned off.");
                        break;
                    case "status":
                    case "s":
                        System.out.println("status: " + this.sensor.getStatus());
                        break;
                    case "update":
                    case "u":
                        this.sensor.update();
                        System.out.println("sensor value refreshed.");
                        break;
                    case "value":
                    case "v":
                        System.out.println("value: " + this.sensor.getValue());
                        break;
                    default:
                        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value");
                        break;
                }
            } catch (SensorNotActivatedException ex) {
                Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
