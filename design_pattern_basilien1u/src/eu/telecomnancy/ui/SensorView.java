package eu.telecomnancy.ui;

import eu.telecomnancy.command.Command;
import eu.telecomnancy.command.CommandeOff;
import eu.telecomnancy.command.CommandeOn;
import eu.telecomnancy.command.CommandeUpdate;
import eu.telecomnancy.sensor.SensorInterface;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SensorView extends JPanel implements Observer{
    private SensorInterface sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(SensorInterface c) {
        this.sensor = c;
        c.attach(this);
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            Command comm = new CommandeOn(c);
            @Override
            public void actionPerformed(ActionEvent e) {
                comm.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            Command comm = new CommandeOff(c);
            @Override
            public void actionPerformed(ActionEvent e) {
                comm.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            Command comm = new CommandeUpdate(c);
            @Override
            public void actionPerformed(ActionEvent e) {
                comm.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    @Override
    public void updateView() {
        try {
            this.value.setText(Double.toString(sensor.getValue()));
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
