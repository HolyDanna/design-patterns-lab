package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorInterface;
import eu.telecomnancy.decorator.FahrenheitDecorator;
import eu.telecomnancy.decorator.FloorDecorator;
import eu.telecomnancy.proxy.SensorProxy;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {

    private SensorInterface sensor;
    private SensorView sensorView;

    public MainWindow(SensorInterface sensor) {
        //this.sensor = sensor;
        this.sensor = new FloorDecorator(new FahrenheitDecorator(sensor));
        this.sensorView = new SensorView(this.sensor);

        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
