/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.SensorInterface;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.Observer;

/**
 *
 * @author holy
 */
public class FloorDecorator  implements SensorInterface {
    
    private SensorInterface sens;
    
    public FloorDecorator(SensorInterface s){
        sens=s;
    }

    @Override
    public void on() {
        sens.on();
    }

    @Override
    public void off() {
        sens.off();
    }

    @Override
    public boolean getStatus() {
        return sens.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sens.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        double value;
        value = sens.getValue();
        value=Math.floor(10*value)/10;
        return value;
    }

    @Override
    public void attach(Observer Viewer) {
        sens.attach(Viewer);
    }

    @Override
    public void detach(Observer Viewer) {
        sens.detach(Viewer);
    }

    @Override
    public void updateViews() {}
    
}
