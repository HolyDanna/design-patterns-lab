/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.SensorInterface;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.Observer;

/**
 *
 * @author holy
 */
public class KelvinDecorator  implements SensorInterface {
    
    private SensorInterface sens;
    
    public KelvinDecorator(SensorInterface s){
        sens=s;
    }

    @Override
    public void on() {
        sens.on();
    }

    @Override
    public void off() {
        sens.off();
    }

    @Override
    public boolean getStatus() {
        return sens.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sens.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        double value;
        if ("CelsiusDecorator".equals(sens.getClass().getName())) {
            value = (sens.getValue()+273.15);
        } else if ("KelvinDecorator".equals(sens.getClass().getName())) {
            value = sens.getValue();
        } else if ("FahrenheitDecorator".equals(sens.getClass().getName())) {
            value = ((sens.getValue()-32)/1.8)+273.15;
        } else {
            value = (sens.getValue()+273.15);
        }
        return value;
    }

    @Override
    public void attach(Observer Viewer) {
        sens.attach(Viewer);
    }

    @Override
    public void detach(Observer Viewer) {
        sens.detach(Viewer);
    }

    @Override
    public void updateViews() {}
    
}
