/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorInterface;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author holy
 */
public class CommandeUpdate implements Command {
    
    private SensorInterface sens;
    
    public CommandeUpdate(SensorInterface sensor){
        this.sens=sensor;
    }
    
    @Override
    public void execute() {
        try {
            sens.update();
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandeUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
