/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.command;

import eu.telecomnancy.sensor.SensorInterface;

/**
 *
 * @author holy
 */
public class CommandeOn implements Command{
    
    private SensorInterface sens;
    
    public CommandeOn(SensorInterface sensor){
        this.sens=sensor;
    }
    
    public void execute(){
        sens.on();
    }
    
}
